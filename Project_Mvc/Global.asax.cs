﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Project_Mvc.DAL;
using Project_Mvc.DAL.UnitOfWork;

namespace Project_Mvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            DataContext data = new DataContext();

            try
            {
                //check if database exists, if not create and insert admin user
                if (!data.Database.Exists())
                {
                    data.Database.Create();
                    DataInitializer di = new DataInitializer();
                    di.InsertAdminUser(data);
                }

                WebMatrix.WebData.WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "UserName", true);
            }
            catch (Exception)
            {   
                throw;
            }
        }

        protected void Application_AuthenticateRequest(HttpContextBase httpContext)
        {
            if (WebMatrix.WebData.WebSecurity.IsAuthenticated)
            {
                bool isConfirmed = WebMatrix.WebData.WebSecurity.IsConfirmed(User.Identity.Name);

                if (isConfirmed == false)
                {
                    WebMatrix.WebData.WebSecurity.Logout();    
                } 
            }
        }
    }

    public class AuthorizeIfConfirmedAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext)) return false;

            System.Security.Principal.IIdentity user = httpContext.User.Identity;
            return WebMatrix.WebData.WebSecurity.IsConfirmed(user.Name);
        }
    }

}