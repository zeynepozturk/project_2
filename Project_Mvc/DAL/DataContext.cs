﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Project_Mvc.DAL
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("ZeynepOzturkProject")
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<MemberStatuAudit> MemberStatuAudit { get; set; }
    }
}
 

 