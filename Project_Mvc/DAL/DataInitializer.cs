﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Mvc.DAL
{
    public class DataInitializer
    {
        public void InsertAdminUser(DataContext context)
        {
            var users = new List<Customer>
            {
                new Customer
                {
                    FirstName = "Zeynep",
                    LastName = "Ozturk",
                    LoginName = "admin",
                    Password = "123", 
                    IsAdmin = true,
                    Email = "zeynoz@gmail.com",
                    PhoneNumber = "555-2132",
                    Address = "Wall Street NY USA",
                    Status = (int)UserStatus.Active,
                }
            };

            users.ForEach(s => context.Customers.Add(s));
            context.SaveChanges();
        }
    }
}