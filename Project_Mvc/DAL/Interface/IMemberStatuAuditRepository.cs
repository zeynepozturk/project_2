﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Mvc.DAL.Interface
{
    interface IMemberStatuAuditRepository : IDisposable 
    {
        IEnumerable<MemberStatuAudit> GetAudits();
        MemberStatuAudit GetAuditByID(int auditId);
        void InsertAudit(MemberStatuAudit audit);
        void DeleteAudit(int auditId);
        void UpdateAudit(MemberStatuAudit audit);
        void Save();
    }
}
