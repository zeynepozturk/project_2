﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Project_Mvc.DAL
{   
        public abstract class BaseModel
        {
            [Key]
            [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
        }

        public enum UserStatus : int
        {
            Suspended = 0,
            Active = 1
        }

        [Table("Customers")]
        public class Customer : BaseModel
        {
            [StringLength(50)]
            [Required]
            public string LoginName { get; set; }
            
            public bool IsAdmin { get; set; }
            
            [StringLength(50)]
            [Required]
            public string FirstName { get; set; }
            
            [StringLength(50)]
            [Required]
            public string LastName { get; set; }
            
            [StringLength(125)]
            [Required]
            [EmailAddress(ErrorMessage="Email is Required")]
            public string Email { get; set; }
            
            [StringLength(30)]
            [Required]
            [Phone]
            public string PhoneNumber { get; set; }
            
            [StringLength(255)]
            [Required]
            public string Address { get; set; }

            public int Status { get; set; }
            
            [StringLength(50)]
            [Required]
            public string Password { get; set; }
        }

        [Table("MemberStatuAudits")]
        public class MemberStatuAudit : BaseModel
        {
            [Required]
            public int MemberId { get; set; }
            [Required]
            public int Status { get; set; }
            [Required]
            public int AdminId { get; set; }
        }
}
