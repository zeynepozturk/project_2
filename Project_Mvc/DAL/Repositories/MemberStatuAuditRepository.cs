﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Project_Mvc.DAL.Interface;

namespace Project_Mvc.DAL.Repositories
{
    public class MemberStatuAuditRepository : IMemberStatuAuditRepository
    {
        private DataContext context;

        public MemberStatuAuditRepository(DataContext context)
        {
            this.context = context;
        }

        public IEnumerable<MemberStatuAudit> GetAudits()
        {
            return context.MemberStatuAudit.ToList();
        }

        public MemberStatuAudit GetAuditByID(int auditId)
        {
            return context.MemberStatuAudit.Find(auditId);
        }

        public void InsertAudit(MemberStatuAudit audit)
        {
            context.MemberStatuAudit.Add(audit);
        }

        public void DeleteAudit(int auditId)
        {
            MemberStatuAudit audit = context.MemberStatuAudit.Find(auditId);
            context.MemberStatuAudit.Remove(audit);
        }

        public void UpdateAudit(MemberStatuAudit audit)
        {
            context.Entry(audit).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
      {
         if (!this.disposed)
         {
            if (disposing)
            {
               context.Dispose();
            }
         }
         this.disposed = true;
      }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}