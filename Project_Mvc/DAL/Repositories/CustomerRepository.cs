﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Project_Mvc.DAL.Interface;

namespace Project_Mvc.DAL.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private DataContext context;

        public CustomerRepository(DataContext context)
        {
            this.context = context;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return context.Customers.ToList();
        }

        public Customer GetCustomerByID(int customerId)
        {
            return context.Customers.Find(customerId);
        }

        public void InsertCustomer(Customer customer)
        {
            context.Customers.Add(customer);
        }

        public void DeleteCustomer(int customerId)
        {
            Customer customer = context.Customers.Find(customerId);
            context.Customers.Remove(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            context.Entry(customer).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
      {
         if (!this.disposed)
         {
            if (disposing)
            {
               context.Dispose();
            }
         }
         this.disposed = true;
      }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}