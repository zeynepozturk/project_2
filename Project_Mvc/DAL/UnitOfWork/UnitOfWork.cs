﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Project_Mvc.DAL.Repositories;
using Project_Mvc.Models;

namespace Project_Mvc.DAL.UnitOfWork
{
    public class UnitOfWork : IDisposable
    {
        private DataContext context = new DataContext();
        private GenericRepository<Customer> customerRepository;
        private GenericRepository<MemberStatuAudit> auditRepository;

        public GenericRepository<Customer> CustomerRepository
        {
            get
            {

                if (this.customerRepository == null)
                {
                    this.customerRepository = new GenericRepository<Customer>(context);
                }
                return customerRepository;
            }
        }

        public GenericRepository<MemberStatuAudit> AuditRepository
        {
            get
            {

                if (this.auditRepository == null)
                {
                    this.auditRepository = new GenericRepository<MemberStatuAudit>(context);
                }
                return auditRepository;
            }
        }

        public List<Customer> GetUsersWithoutAdmin()
        {
            return this
                .CustomerRepository
                .Get(filter: f => f.LoginName != "admin")
                .ToList();
        }

        public void Kickout(string loginname, bool activate, string loggedUserName)
        {
            int newStatus = activate == false ? (int)UserStatus.Suspended : (int)UserStatus.Active;

            Customer customer = this.CustomerRepository
                .Get(filter: c => c.LoginName == loginname).FirstOrDefault();

            if (customer == null)
            {
                return;
            }

            customer.Status = newStatus;
            this.CustomerRepository.Update(customer);
            this.Save();

            string username = loginname;
            AppGlobal.LoggedUsers.TryRemove(username, out username);

            Customer adminUser = this.CustomerRepository
                .Get(filter: c => c.LoginName == loggedUserName).FirstOrDefault();

            DAL.MemberStatuAudit log = new DAL.MemberStatuAudit();

            log.AdminId = adminUser.Id;
            log.MemberId = customer.Id;


            log.Status = newStatus;

            this.AuditRepository.Insert(log);

            this.Save();
        }

        public void Register(CustomerModel customerModel)
        {
            Mapper.CreateMap<CustomerModel, Customer>();
            Customer customer = Mapper.Map<CustomerModel, Customer>(customerModel);

            customer.IsAdmin = false;
            customer.Status = (int)UserStatus.Active;

            this.CustomerRepository.Insert(customer);
            this.Save();

        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}