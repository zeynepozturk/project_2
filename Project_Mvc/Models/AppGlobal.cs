﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace Project_Mvc.Models
{
    public class AppGlobal
    {
        public static ConcurrentDictionary<string, string> LoggedUsers
        {
            get
            {
                if (HttpContext.Current.Application["loggedUsers"] == null)
                {
                    HttpContext.Current.Application["loggedUsers"] = new ConcurrentDictionary<string, string>();
                }

                ConcurrentDictionary<string, string> dict = (ConcurrentDictionary<string, string>)HttpContext.Current.Application["loggedUsers"];

                return dict;
            }
        }
    }
}