﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace Project_Mvc.Models
{
    public class AppGlobals
    {
        public static ConcurrentBag<string> LoggedUsers
        {
            get
            {
                if (HttpContext.Current.Application["loggedUsers"] == null)
                {
                    HttpContext.Current.Application["loggedUsers"] = new ConcurrentBag<string>();
                }

                ConcurrentBag<string> dict = (ConcurrentBag<string>)HttpContext.Current.Application["loggedUsers"];

                return dict;
            }
        }
    }
}