﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Mvc.Models
{
    public class UserLoginModel
    {
        [StringLength(50)]
        [Required(ErrorMessage = "Login Name is required...")]
        public string LoginName { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Password is required...")]
        public string Password { get; set; }
    }
}