﻿using System.ComponentModel.DataAnnotations;

namespace Project_Mvc.Models
{
    public class CustomerModel : Project_Mvc.DAL.Customer
    {
        [Compare("Password", ErrorMessage="Passwords must match")]
        [StringLength(50)]
        [Required]
        public string PasswordConfirm { get; set; }
    }
}