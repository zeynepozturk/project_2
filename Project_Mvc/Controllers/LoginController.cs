﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_Mvc.DAL;
using Project_Mvc.DAL.UnitOfWork;
using System.Web.Security;
using Project_Mvc.Models;

namespace Project_Mvc.Controllers
{
    public class LoginController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserLoginModel user)
        {
            if (ModelState.IsValid)
            {
                Customer customer = unitOfWork.CustomerRepository.Get(filter: x => x.LoginName == user.LoginName && x.Password == user.Password).FirstOrDefault();

                if (customer != null)
                {
                    //non-admin users are not allowed to logged in if they are suspended
                    if (!customer.IsAdmin && customer.Status == (int)UserStatus.Suspended)
                    {
                        ModelState.AddModelError("", "please contact customer service");
                        return View();
                    }

                    FormsAuthentication.SetAuthCookie(customer.LoginName, true);
                    
                    //Users with active status are added to application cache user list when they logged in
                    AppGlobal.LoggedUsers.TryAdd(customer.LoginName, customer.LoginName);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }

            return View(user);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            //Users are removed from application cache user list when they logged out
            string username = HttpContext.User.Identity.Name;
            AppGlobal.LoggedUsers.TryRemove(username, out username);

            FormsAuthentication.SignOut();
            
            return RedirectToAction("Index", "Home");
        }

    }
}
