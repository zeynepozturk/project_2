﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_Mvc.DAL;
using Project_Mvc.DAL.UnitOfWork;
using Project_Mvc.Filters;
using Project_Mvc.Models;

namespace Project_Mvc.Controllers
{
    [CustomActionFilter]
    [Authorize(Users="admin")]
    public class AuditController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet]
        public ActionResult Index(string loginname = "", bool activate = false)
        {
            if (!string.IsNullOrEmpty(loginname))
            {
                unitOfWork.Kickout(loginname, activate, HttpContext.User.Identity.Name);
            }

            //returns only logged users from cache
            List<Customer> customers = unitOfWork.GetUsersWithoutAdmin();

            return View(customers);
        }
    }
}
