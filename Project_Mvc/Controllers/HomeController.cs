﻿using System.Web.Mvc;
using Project_Mvc.DAL.UnitOfWork;
using Project_Mvc.Filters;

namespace Project_Mvc.Controllers
{
    [CustomActionFilter]
    [Authorize]
    public class HomeController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
