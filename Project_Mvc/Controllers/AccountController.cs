﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Project_Mvc.DAL;
using Project_Mvc.DAL.UnitOfWork;
using Project_Mvc.Filters;
using Project_Mvc.Models;

namespace Project_Mvc.Controllers
{ 
    public class AccountController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet]
        public ActionResult Register()
        {
            return View(new CustomerModel());
        }

        [HttpPost]
        public ActionResult Register(CustomerModel customerModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    unitOfWork.Register(customerModel);
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }

                return RedirectToAction("Index", "Home");
            }

            return View(customerModel);
        }
    }
}
