﻿using Project_Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Project_Mvc.Filters
{
    public class CustomActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string loginName = filterContext.HttpContext.User.Identity.Name;

            //if user is already logged, check kickoff
            if (!string.IsNullOrEmpty(loginName))
            {
                string nameFound;
                AppGlobal.LoggedUsers.TryGetValue(loginName, out nameFound);

                if (string.IsNullOrEmpty(nameFound))
                {
                    FormsAuthentication.SignOut();

                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Index" }, { "controller", "Login" } });

                    return;
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}